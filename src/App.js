// @flow

import { AppNode } from "vasille-js";


// Define the hello world app
export class App extends AppNode {

    // Defines a public property
    name = this.$public(String);

    constructor () {
        super(document.body, {debug: true});
    }

    // Create DOM of component
    $createDom () {
        // create a <main> tag
        this.$defTag('main', main => {
            // create a <h1> tag
            main.$defTag('h1', h1 => {
                h1.$defText(this.$bind(name => `Hello ${name}!`, this.name));
            });
            // create a <p> tag
            main.$defTag('p',  p=> {
                p.$defText("Welcome in my world :)")
            });
        });
    }
}
