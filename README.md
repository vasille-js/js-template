# Vasille JS App

This is a template for [Vasille JS](https://gitlab.com/vasille-js/vasille-js)
apps. It lives at https://gitlab.com/vasille-js/js-template.

To create a project based on this project using degit:
```bash
npx degit gitlab:vasille-js/js-template vasille-app
cd vasille-app
```

*[Node.js](https://nodejs.org) must be installed*.

## Get started

Install dev dependencies...
```
npm install
```

... then start [webpack](https://webpack.js.org/)

```
npm run serve
```

Navigate to [localhost:3000](http://localhost:3000), you should see your app running. 
Edit the component file `App.js` in `src`, save it, the changes will be reloaded automatically.

*The style file `style.css` and HTML file `index.html` are placed in `dist` directory.*

## Building and running in production mode

Run `npm run build` to build your app, the release files will be placed in `dist` directory,
to serve it, you can use [serve](https://www.npmjs.com/package/serve) package...

```
npm install -g serve
cd dist
serve .
```

or deploy to any server, including HTML only hosts like GitHub/GitLab pages.

## Result

A deployed version of this project can be accesed [here](https://vasille-js.gitlab.io/js-template/).
