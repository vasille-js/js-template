const path = require ("path");

module.exports = {
    mode : "production",
    entry : "./src/main.js",
    output : {
        filename : "app.js",
        path : path.resolve (__dirname, "dist")
    },
    module : {
        rules : [
            {
                test : /\.js$/,
                use : "babel-loader"
            }
        ]
    },
    stats : {
        colors : true
    },
    optimization : {
        minimize : true
    }
};
